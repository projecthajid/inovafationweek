import Topbar from "../../component/user/topbar"
import Sidebar from "../../component/user/sidebar"
import Content1 from "../../component/user/indexUser"

export default function Home() {
  return (
<div className="fixed-left widescreen">

{/* <!-- Begin page --> */}
<div id="wrapper">

    {/* <!-- Top Bar Start --> */}
    <div className="topbar">
        {/* <!-- LOGO --> */} 
        <div className="topbar-left" >
            
              <Topbar/>
        </div>
    </div>
    {/* <!-- Top Bar End --> */}


    {/* <!-- ========== Left Sidebar start ========== --> */}

    <div className="left side-menu" style={{boxShadow: '2px 0px 5px 0px rgba(199,191,199,1)'}}>
        <div className="sidebar-inner slimscrollleft">
            {/* <!--- Divider --> */}
            <div id="sidebar-menu">
            
                <Sidebar/>
            </div>
        </div>
    </div>
    {/* <!-- ========== Left Sidebar end ========== --> */}

    {/* <!-- ========== main content start ========== --> */}
    <div className="content-page">

        {/* <!-- content --> */}
        <div className="content" style={{ minHeight: 900, backgroundColor: '#E5EAE5 ' }}>
        <Content1 />
        </div>
        
        {/* <!-- content --> */}

        {/* <!-- footer --> */}
        <footer className="footer" style={{}}>
            © 2022 MyVoucher - All Rights Reserved.
        </footer>
    </div>

    {/* <!-- ========== main content end ========== --> */}


</div>


</div>
  )
}
