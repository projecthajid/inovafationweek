import Topbar from "../../component/admin/topbar"
import Sidebar from "../../component/admin/sidebar"
import TambahData from "../../component/admin/insertDataP"


export default function tambah() {
  return (
<div className="fixed-left widescreen">

{/* <!-- Begin page --> */}
<div id="wrapper">

    {/* <!-- Top Bar Start --> */}
    <div className="topbar">
        {/* <!-- LOGO --> */} 
        <div className="topbar-left" >
            
              <Topbar/>
        </div>
    </div>
    {/* <!-- Top Bar End --> */}


    {/* <!-- ========== Left Sidebar start ========== --> */}

    <div className="left side-menu" style={{boxShadow: '2px 0px 5px 0px rgba(199,191,199,1)'}}>
        <div className="sidebar-inner slimscrollleft">
            {/* <!--- Divider --> */}
            <div id="sidebar-menu">
            
                <Sidebar/>
            </div>
        </div>
    </div>
    {/* <!-- ========== Left Sidebar end ========== --> */}

    {/* <!-- ========== main content start ========== --> */}
    <div className="content-page">

        {/* <!-- content --> */}
        <div className="content" style={{ minHeight: 950, backgroundColor: '#E5EAE5 ' }}>
       <TambahData />
        </div>
        
        {/* <!-- content --> */}

        {/* <!-- footer --> */}
        <footer className="footer" style={{}}>
            © 2022 MyVoucher - All Rights Reserved.
        </footer>
    </div>

    {/* <!-- ========== main content end ========== --> */}


</div>


</div>
  )
}
