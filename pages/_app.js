import '../styles/globals.css'
import '../styles/Home.css'
import 'bootstrap/dist/css/bootstrap.css';



import "@fortawesome/fontawesome-svg-core/styles.css";
import { config } from "@fortawesome/fontawesome-svg-core";
config.autoAddCss = false;

import { useEffect } from 'react';


function MyApp({ Component, pageProps }) {
  useEffect(()=>{
    import('bootstrap')
  },[])

  return <Component {...pageProps} />
}

export default MyApp
