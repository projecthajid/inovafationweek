import Header from "../component/landingPage/header";
import Opening from "../component/landingPage/opening";
import AppFeatures from "../component/landingPage/appFeatures";
import Contact from "../component/landingPage/contact";
import Footer from "../component/landingPage/footer";


export default function Home() {
  return (
<>
<Header />
<main>
<Opening/>
<AppFeatures/>

<Footer />

</main>
</>
  )
}
