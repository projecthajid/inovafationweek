import Link from 'next/link';
function Header() {

    return (
<>
<header id="header" className="fixed-top  header-transparent ">
    <div className="container d-flex align-items-center justify-content-between">

      <div className="logo">
       
       
        <a href="index.html"><img src="/logo/logoMyVoucherBiru.jpg"  alt="" className="img-fluid" /></a>
      </div>

      <nav id="navbar" className="navbar">
        <ul>
          <li><a className="nav-link scrollto active" href="#hero">Home</a></li>
          <li><a className="nav-link scrollto" href="#features">App Features</a></li>
          <li><a className="nav-link scrollto" href="#footer">Contact</a></li>
          <li><a className="getstarted scrollto" href="/daftar" style={{textDecoration:'none'}}>Get Started</a></li>
        </ul>
        <i className="bi bi-list mobile-nav-toggle"></i>
      </nav>

    </div>
  </header>
</>
   );
  }
export default Header