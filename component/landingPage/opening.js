import Link from 'next/link';
function Opening() {

    return (
<>
<section id="hero" className="d-flex align-items-center">

<div className="container">
  <div className="row">
    <div className="col-lg-6 d-lg-flex flex-lg-column justify-content-center align-items-stretch pt-5 pt-lg-0 order-2 order-lg-1" data-aos="fade-up">
      <div>
        <h1>MY voucher</h1>
        <h2>Langganan anda ketika malas mu menimpa</h2>
        <Link href="/loginUser"><a  className="button-login-index"> Login</a></Link>
        
        
      </div>
    </div>
    <div className="col-lg-6 d-lg-flex flex-lg-column align-items-stretch order-1 order-lg-2 hero-img " data-aos="fade-up">
      <img src="/animasi/animasi.png" className="img-fluid" style={{maxHeight:600}} alt=""/>
    </div>
  </div>
</div>

</section>
</>
   );
  }
export default Opening