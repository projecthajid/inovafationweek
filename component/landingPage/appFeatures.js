import Link from 'next/link';
function AppFeatures() {

    return (
<>
<section id="features" className="features"  style={{backgroundColor: '#D3DCF4 '}}>
      <div className="container">

        <div className="section-title">
          <h2>App Features</h2>
          <p>Kami memberikan kemudahan bagi anda yang sedang dalam keadaan bad mood apalagi kalian-kalian yang galau sedang mager males  kemana mana,
                    yang tak mau beranjak walau hati hancur terinjak. mood hancur tanpa semangat darinya lagi. </p>
        </div>
      </div>
    </section>
    
    <section id="details" className="details"style={{backgroundColor: '#D3DCF4  '}}>
      <div className="container">

        <div className="row content">
          <div className="col-md-4" data-aos="fade-right">
            <img src="/animasi/1.png" className="img-fluid" alt=""/>
          </div>
          <div className="col-md-8 pt-4" data-aos="fade-up">
            <h3>Bebas pilih Prvider apa kamu mau.</h3>
            <p className="fst-italic">
              Terserah kamu mau pilih mana aku, dia apa mereka. Yang jelas kami setia memberikan apa yang ku punya
            </p>
            <ul>
              <li> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
              <li> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
              <li> Iure at voluptas aspernatur dignissimos doloribus repudiandae.</li>
              <li> Est ipsa assumenda id facilis nesciunt placeat sed doloribus praesentium.</li>
            </ul>
            <p>
              Voluptas nisi in quia excepturi nihil voluptas nam et ut. Expedita omnis eum consequatur non. Sed in asperiores aut repellendus. Error quisquam ab maiores. Quibusdam sit in officia
            </p>
          </div>
        </div>

        <div className="row content" >
          <div className="col-md-4 order-1 order-md-2" data-aos="fade-left">
            <img src="/animasi/2.png" className="img-fluid" style={{height:500}} alt=""/>
          </div>
          <div className="col-md-8 pt-5 order-2 order-md-1" data-aos="fade-up">
            <h3>Murah dan setia dengan semua jenis dompet kamu</h3>
            <p className="fst-italic">
              Terserah kamu mau pilih mana aku, dia apa mereka. Yang jelas kami setia memberikan apa yang ku punya
            </p>
            <p>
              Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
              velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
              culpa qui officia deserunt mollit anim id est laborum
            </p>
            <ul>
              <li> Et praesentium laboriosam architecto nam .</li>
              <li> Eius et voluptate. Enim earum tempore aliquid. Nobis et sunt consequatur. Aut repellat in numquam velit quo dignissimos et.</li>
              <li> Facilis ut et voluptatem aperiam. Autem soluta ad fugiat.</li>
            </ul>
          </div>
        </div>

         <div className="row content">
          <div className="col-md-4" data-aos="fade-right">
            <img src="/animasi/3.png" className="img-fluid" alt=""/>
          </div>
          <div className="col-md-8 pt-4" data-aos="fade-up">
            <h3>Bebas pilih Prvider apa kamu mau.</h3>
            <p className="fst-italic">
              Terserah kamu mau pilih mana aku, dia apa mereka. Yang jelas kami setia memberikan apa yang ku punya
            </p>
            <ul>
              <li> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
              <li> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
              <li> Iure at voluptas aspernatur dignissimos doloribus repudiandae.</li>
              <li> Est ipsa assumenda id facilis nesciunt placeat sed doloribus praesentium.</li>
            </ul>
            <p>
              Voluptas nisi in quia excepturi nihil voluptas nam et ut. Expedita omnis eum consequatur non. Sed in asperiores aut repellendus. Error quisquam ab maiores. Quibusdam sit in officia
            </p>
          </div>
        </div>


      </div>
    </section>
</>
   );
  }
export default AppFeatures