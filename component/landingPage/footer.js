import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {  } from "@fortawesome/free-solid-svg-icons";
import { faFacebook, faFacebookSquare, faGooglePlus, faInstagram, faInstagramSquare, faLinkedin, faTwitter } from "@fortawesome/free-brands-svg-icons";
import Link from 'next/link';

function Footer() {

    return (
<>
<footer id="footer">

<div className="footer-newsletter">
  <div className="container">
    <div className="row justify-content-center">
      <div className="col-lg-6">
        <h4>Join Our Newsletter</h4>
        <p>المحتويات هي تطبيق ويب لشراء قسائم حزم البيانات التي تم الوثوق بها منذ عصر الإبل</p>
        <form action="" method="post">
          <input type="email" name="email"/><input type="submit" value="Subscribe"/>
        </form>
      </div>
    </div>
  </div>
</div>

<div className="footer-top">
  <div className="container">
    <div className="row">

      <div className="col-lg-3 col-md-6 footer-contact">
        <h3>MyVoucher</h3>
        <p>
         jatiBening - kaliMalang <br/>
          Bekasi,  535022<br/>
          republikIndonesia <br/><br/>
          <strong>Phone:</strong> +62 5589 55488 55<br/>
          <strong>Email:</strong> faizhajid57@gmail.com<br/>
        </p>
      </div>

      <div className="col-lg-3 col-md-6 footer-links">
        <h4>We are here</h4>
        <ul>
          <li><i className="bx bx-chevron-right"></i> <a >Home</a></li>
          <li><i className="bx bx-chevron-right"></i> <a>About us</a></li>
          <li><i className="bx bx-chevron-right"></i> <a>Privacy policy</a></li>
        </ul>
      </div>

      <div className="col-lg-3 col-md-6 footer-links">
        <h4>Our Services</h4>
        <ul>
          <li> <a>TELKOMSEL</a></li>
          <li> <a>SMARTFREN</a></li>
          <li> <a>AXIS</a></li>
          <li> <a>INDOSAT</a></li>
        </ul>
      </div>

      <div className="col-lg-3 col-md-6 footer-links">
        <h4>Our Social Networks</h4>
        <p>we make it easy for you to contact us wherever you go, the important thing is that you come back </p>
        <div className="social-links mt-3">
          <a  className="twitter"><FontAwesomeIcon icon={faTwitter} /> </a>
          <a  className="facebook"><FontAwesomeIcon icon={faFacebook}/></a>
          <a  className="instagram"><FontAwesomeIcon icon={faInstagram}/></a>
          <a  className="google-plus"><FontAwesomeIcon icon={faGooglePlus}/></a>
          <a  className="linkedin"><FontAwesomeIcon icon={faLinkedin}/></a>
        </div>
      </div>

    </div>
  </div>
</div>

<div className="container py-4">
  <div className="copyright">
    &copy; Copyright <strong><span>MyVoucher</span></strong>. All Rights Reserved
  </div>
  <div className="credits">

    Designed by <a href="/loginAdmin">MyVoucher Suport System</a>
  </div>
</div>
</footer>
</>
   );
  }
export default Footer