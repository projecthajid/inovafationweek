import Link from "next/link";
import { useState } from "react";
import { useRouter } from "next/router"
import axios from "axios";

const TambahData = () => {

    const [nama, setProfider] = useState([])
    const [gb, setGb] = useState([])
    const [harga, setHarga] = useState([])
    const [info, setInfo] = useState([])
    const [code, setCode] = useState([])

    const onChangeNamaProfider = (e) => {
        const value = e.target.value
        setProfider(value)
    }

    const onChangeKategori = (e) => {
        const value = e.target.value
        console.log(value)
        setGb(value)
    }

    const onChangeHarga = (e) => {
        const value = e.target.value
        setHarga(value)
    }

    const onChangeInfo = (e) => {
        const value = e.target.value
        setInfo(value)
    }

    const onChangeCode = (e) => {
                    const value = e.target.value
                    setCode(value)
                }



    const onInsert = (e) => {
        e.preventDefault()
    }

    const router = useRouter()

    const InsertSubmit = async () => {

        try {
            setProfider(nama)
            setGb(gb)
            setHarga(harga)
            setInfo(info)
            setCode(code)
            const res = await axios.post("http://localhost:3001/product/add-product", {nama, gb, harga, info ,code})
            .then(result => {
                console.log(result.data.statusCode)
                if (result.data.statusCode == 201 || result.data.statusCode == 200) {
                    window.alert(result.data.message)
                    router.push('/admin/detailProduct')
                } else {
                    window.alert("Gagal ")
                }
            })
        } catch (error) {
            console.error(error);
        }

    }



return (
<>
<div className="row">
                                <div className="col-md-12 col-xl-6">
                                    <div className="card m-b-30">
                                        <div className="card-body">

<h4 className="mt-0 header-title">Tambah produk disini</h4>
                                       

<form onSubmit={onInsert} method="POST">

                                                 <div className="form-group mt-3 ">
                                                    <label>Profider</label>
                                                    <select  className="form-control" id="inlineFormCustomSelect" onChange={onChangeNamaProfider}>
                                                            <option selected>pilih Profider</option>
                                                            <option value={'TELKOMSEL'} >TELKOMSEL</option>
                                                            <option  value={'AXIS'} >AXIS</option>
                                                            <option value={'INDOSAT'} >INDOSAT</option>
                                                            <option value={'SMARTFREN'} >SMARTFREN</option>
                                                    </select>
                                                </div>

                                                <div className="form-group mt-3 ">
                                                    <label>kategori</label>
                                                    <select  className="form-control" id="inlineFormCustomSelect"  onChange={onChangeKategori} >
                                                            <option selected>mau berapa giga</option>
                                                            <option value={'3 gb'} >3 gb</option>
                                                            <option  value={'5 gb'} >5 gb</option>
                                                            <option value={'8 gb'} >8 gb</option>
                                                            <option value={'10 gb'} >10 gb</option>
                                                            <option  value={'15 gb'} >15 gb</option>
                                                            <option value={'20 gb'} >20 gb</option>
                                                    </select>
                                                </div>

                                                <div className="form-group mt-3 ">
                                                    <label>Harga</label>
                                                    <input type="text" name="harga" value={harga} onChange={onChangeHarga} className="form-control" placeholder="contoh penulisan 50000"></input>    
                                                </div>

                                                <div className="form-group mt-3 ">
                                                    <label>Info</label>
                                                    <textarea type="text" name="info" value={info} onChange={onChangeInfo} className="form-control" placeholder="infonya apa ni "></textarea>    
                                                </div>

                                                <div className="form-group mt-3 ">
                                                    <label>Code</label>
                                                    <textarea type="text" name="code" value={code} onChange={onChangeCode} className="form-control" placeholder="infonya apa ni "></textarea>    
                                                </div>
            
 
                                                <div className="form-group mb-2 mt-5 ">
                                                    <div>
                                                        <button  onClick={InsertSubmit} type="submit" className="btn btn-secondary waves-effect waves-light me-3">
                                                            Save
                                                        </button>
                                                        <button type="reset" className="btn btn-danger waves-effect ">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
 </div>
 </div></div>
 </div>                                           
</>
);
}
export default TambahData              