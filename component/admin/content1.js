import Image from "next/image";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalculator, faDollarSign, faUserAstronaut } from "@fortawesome/free-solid-svg-icons";

import { useEffect, useState } from "react";

const Content1 = () => {
  const[jumlahUser, setUser] = useState([]);
  const apiUser ="http://localhost:3001/users";
  useEffect(()=> {

     
      getUser();
  },[] );
  const getUser = async () => {
        
      const res = await axios.get(apiUser);
      setUser([res.data.data]);
    };

  const[jumlahProduk, setProduk] = useState([]);
  const apiProduk ="http://localhost:3001/product/get-all-product";
  useEffect(()=> {

     
      getProduk();
  },[] );
  console.log(jumlahProduk)
  const getProduk = async () => {
        
      const res = await axios.get(apiProduk);
      setProduk([res.data]);
    };

    return (
<>
<div className="row" ></div>
<div className="row">
  <div className="col-md-12 col-xl-3">
    <div className="card mini-stat" style={{ boxShadow: '0px 10px 5px 0px rgba(199,191,199,1)'}}>
      <div className="mini-stat-icon text-right">
        <i className="mdi mdi-cube-outline" style={{float:'right'}}>
          <FontAwesomeIcon
          icon={faUserAstronaut}
          />
        </i>
      </div>
      <div className="p-4"><h5 className="text-uppercase">Total User</h5>
      {/* {dataUsers.map((jumlah) => ( */}
        <h4 className="">{jumlahUser.length}</h4>
       {/* ) )}  */}
        <h6>*user di sini</h6>
      </div>
    </div>
  </div>
  <div className="col-md-12 col-xl-3">
    <div className="card mini-stat" style={{ boxShadow: '0px 10px 5px 0px rgba(199,191,199,1)'}}>
      <div className="mini-stat-icon text-right">
        <i className="mdi mdi-cube-outline" style={{float:'right'}}>
          <FontAwesomeIcon
          icon={faCalculator}
          />
        </i>
      </div>
      <div className="p-4"><h5 className="text-uppercase"> Penjualan</h5>        
      <h4 className="">0 </h4>
       
        <h6>*barang sudah terjual</h6>
      </div>
    </div>
  </div>
  <div className="col-md-12 col-xl-3">
    <div className="card mini-stat" style={{ boxShadow: '0px 10px 5px 0px rgba(199,191,199,1)'}}>
      <div className="mini-stat-icon text-right">
        <i className="mdi mdi-cube-outline" style={{float:'right'}}>
          <FontAwesomeIcon
          icon={faDollarSign}
          />
        </i>
      </div>
      <div className="p-4"><h5 className="text-uppercase">Total Dana</h5>
        <h4 className="">0 </h4><h6>*uang milik kita</h6>
      </div>
    </div>
  </div>
  <div className="col-md-12 col-xl-3">
    <div className="card mini-stat" style={{ boxShadow: '0px 10px 5px 0px rgba(199,191,199,1)'}}>
      <div className="mini-stat-icon text-right">
        <i className="mdi mdi-cube-outline" style={{float:'right'}}>
          <FontAwesomeIcon
          icon={faDollarSign}
          />
        </i>
      </div>
      <div className="p-4"><h5 className="text-uppercase">Total produk</h5>
      {jumlahProduk.map((jumProduk) => (
         <h4 key={jumProduk.id}  className="">{jumProduk.count} </h4>
          ))}
        <h6>*jualan kita</h6>
      </div>
    </div>
  </div>


  

</div>    

<div className="row">
                                            <div className="col-md-12 col-xl-8">
                                                <div className="card">
                                                    <img src="brand/telkomsel.jpg" />
                                                 </div>
                                            </div> 
                                            <div className="col-md-12 col-xl-4">
                                    <div className="card">
                                        <div className="card-body">
                                            <h4 className="mt-0 mb-3 header-title">PRODUK KITA</h4> 
                                            <div id="morris-donut-example" style={{height: 320}}>
                                              <ul>
                                                <li className="text-uppercase">
                                                  telkomsel
                                                </li>
                                                <li className="text-uppercase">
                                                  indosat
                                                </li>
                                                <li className="text-uppercase">
                                                  axis
                                                </li>
                                                <li className="text-uppercase">
                                                  smartfren
                                                </li>
                                              </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                        
                                        </div>
</>
   );
  }
export default Content1 //card yang di index

