import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { useRouter } from "next/router"
import { useEffect, useState } from "react";

const TambahUser = () => {

    const [nama, setNama] = useState([])
    const [email, setEmail] = useState([])
    const [wa, setNotlp] = useState([])
    const [password, setPassword] = useState([])
    const [roleId, setRoleid] = useState([])

    const onChangeNama = (e) => {
        const value = e.target.value
        setNama(value)
    }

    const onChangeEmail = (e) => {
        const value = e.target.value
        setEmail(value)
    }

    const onChangeNoTelephon = (e) => {
        const value = e.target.value
        setNotlp(value)
    }

    const onChangePassword = (e) => {
        const value = e.target.value
        setPassword(value)
    }



    const onSubmit = (e) => {
        e.preventDefault()
    }

    const router = useRouter ()

    const registerSubmit = async () => {

        try {
            setNama(nama)
            setEmail(email)
            setNotlp(wa)
            setPassword(password)
            const res = await axios.post("http://localhost:3001/auth/register", {nama, email, wa, password})
            .then(result => {
                console.log(result.data.statusCode)
                if (result.data.statusCode == 201 || result.data.statusCode == 200) {
                    window.alert(result.data.message)
                    router.push('/admin/totalUser')
                } else {
                    window.alert("Gagal")
                }
            })
        } catch (error) {
            console.error(error);
        }

    }



return (
<>
<div className="row">
                                <div className="col-md-12 col-xl-6">
                                    <div className="card m-b-30">
                                        <div className="card-body">

<h4 className="mt-0 header-title">Tambah User disini</h4>
                                       

<form onSubmit={onSubmit} method="POST">
                             

                                                <div className="form-group mt-3 ">
                                                    <label>Username</label>
                                                    <input type="text"  onChange={onChangeNama} name="nama" className="form-control" placeholder="Username"></input>    
                                                </div>
                                                <div className="form-group mt-3 ">
                                                    <label>whatsapp</label>
                                                    <input type="phone"  onChange={onChangeNoTelephon} name="wa" className="form-control" placeholder="whats app"></input>    
                                                </div>
                                                <div className="form-group mt-3 ">
                                                    <label>Email</label>
                                                    <input type="email" onChange={onChangeEmail} name="email" className="form-control" placeholder="Email"></input>    
                                                </div>
                                                <div className="form-group mt-3 ">
                                                    <label>Password</label>
                                                    <input type="password"  onChange={onChangePassword} name="password" className="form-control" placeholder="****"></input>    
                                                </div>
            
 
                                                <div className="form-group mb-2 mt-5 ">
                                                    <div>
                                                        <button type="submit"   onClick={registerSubmit} className="btn btn-secondary btn-success waves-effect waves-light me-3">
                                                            Save
                                                        </button>
                                                        <button type="reset" className="btn btn-danger waves-effect ">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
 </div>
 </div></div>
 </div>                                           
</>
);
}
export default TambahUser              