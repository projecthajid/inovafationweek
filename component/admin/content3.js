import axios from "axios";
import { useEffect, useState } from "react";


const TableDataUser = () => { 
    const[tableUser, setTableUser] = useState([]);
    const apiUser ="http://localhost:3001/users";
    useEffect(()=> {

        console.log(tableUser);
        getTableUser();
    },[] );
    const getTableUser = async () => {
        
        const res = await axios.get(apiUser);
        setTableUser([res.data]);
      };
      console.log(tableUser)
    const deleteUser = async () => {
        window.alert("user ini aktif")
    }

return (
<>
         <div className="row" style={{marginTop: 115}}></div>
         <div className="row">
                                <div className="col-md-12">
                                    <div className="card">
                                        <div className="card-body">
            
                                            <h4 className="mt-0 header-title">User Table</h4>
                                            <p className="text-muted  font-14">{tableUser.length} user ada disini
                                            </p>
            
                                            <table id="datatable-buttons" className="table table-striped table-bordered dt-responsive nowrap" >

                                                <thead>
                                                <tr>
                                                    <th>no</th>
                                                    <th>userName</th>
                                                    <th>email</th>
                                                    <th>password</th>
                                                    <th>whatsapp</th>
                                                    <th>date update </th>
                                                    <th>status</th>
                                                </tr>
                                                </thead>            
                                                <tbody>
                                                {tableUser.map((user) => (
                                                <tr key={user.id} >
                                                    
                                                    <td>1</td>
                                                    <td>{user.nama}</td>
                                                    <td>{user.email}</td>       
                                                    <td>*****</td>
                                                    <td>{user.wa}</td>
                                                    <td>{"12-12-2022"}</td>
                                                    <td><button type="" style={{backgroundColor:'green',color:'white'}} onClick={deleteUser} className="btn ">Active</button></td>
                                                </tr>
                                               
                                               ) )}
                                                 </tbody> 
                                            </table>
            
                                        </div>
                                    </div>
                                </div>
                            </div> 

</>
);
}


export default TableDataUser  


