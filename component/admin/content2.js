import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";



function Content2() {
    const[tableProduk, setTableProduk] = useState([]);
    const apiProduk ="http://localhost:3001/product/get-all-product";
    useEffect(()=> {

        console.log(tableProduk);
        getTableProduk();
    },[] );
    const getTableProduk = async () => {
        
        const res = await axios.get(apiProduk);
        setTableProduk(res.data.data);
      };

      const router = useRouter()
  
     

return (
<>
         <div className="row" style={{marginTop: 115}}></div>
         <div className="row">
                                <div className="col-md-12">
                                    <div className="card">
                                        <div className="card-body">
            
                                            <h4 className="mt-0 header-title">Product Table</h4>
                                            <p className="text-muted  font-14">{tableProduk.length} product ada disini
                                            </p>
            
                                            <table id="datatable-buttons" className="table table-striped table-bordered dt-responsive nowrap" >

                                                <thead>
                                                <tr>
                                                    <th>no</th>
                                                  
                                                  
                                                    <th>Provider</th>
                                                    <th>Categori</th>
                                                    <th>Prize</th>
                                                    <th>Detail</th>
                                                    
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
            
            
                                                <tbody>
                                                {tableProduk.map((produk) => (
                                                <tr key={produk.id} >
                                                    <td>1</td>
                                                    <td>{produk.nama}</td>   
                                                 
                                                     
                                                    <td>{produk.gb}</td>
                                                    <td>{produk.harga}</td>
                                                    <td>{produk.info}</td>
                                                
                                                    <td><button  className="btn btn-danger" onClick={
                                                        async () => {

                                                            try {
                                                            
                                                                const res = await axios.delete(`http://localhost:3001/product/delete-product/${produk.id}`)
                                                                .then(result => {
                                                                    console.log(result.data.statusCode)
                                                                    if (result.data.statusCode == 201 || result.data.statusCode == 200) {
                                                                        window.alert("yakin ingin menghapus")
                                                                        router.push('/admin/')
                                                                    } else {
                                                                        window.alert("Gagal ")
                                                                    }
                                                                })
                                                            } catch (error) {
                                                                console.error(error);
                                                            }
                                                    
                                                        }
                                                    }>Hapus</button></td>
                                                </tr>
                                                
                                                ))}
                                                </tbody>
                                            </table>
            
                                        </div>
                                    </div>
                                </div>
                            </div> 

</>
);
}
export default Content2              