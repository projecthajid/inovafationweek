import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faSearch } from "@fortawesome/free-solid-svg-icons";

function Topbar() {

    return (
<>
<div className="topbar"  >
<nav className="navbar navbar-expand-lg navbar-light" style={{background:'#031554',boxShadow:  ' 0px 3px 13px 2px rgba(149,132,207,0.92)'}}>
  {/* <!-- Container wrapper --> */}
  <div className="container-fluid" >
    {/* <!-- Toggle button --> */}
    <button
      className="navbar-toggler"
      type="button"
      data-mdb-toggle="collapse"
      data-mdb-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
      
    >
      <i className="fas fa-bars"></i>
    </button>

    {/* <!-- Collapsible wrapper --> */}
    <div className="collapse navbar-collapse" id="navbarSupportedContent" >
      {/* <!-- Navbar brand --> */}
      <div className="navbar-brand  mt-lg-0" style={{height:50 }}>

      </div>

    </div>
    {/* <!-- Collapsible wrapper --> */}

    {/* <!-- Right elements --> */}
    <div className="d-flex align-items-center" >
    
      {/* <!-- Avatar --> */}
     
     
    
      <div className=" dropstart"  >
  <button type="button"  className="  " data-bs-toggle="dropdown" aria-expanded="" style={{background:'#031554'}}>
  <img className="rounded-circle" src="https://scontent-cgk1-1.xx.fbcdn.net/v/t1.6435-9/136425034_240738377418841_5480728684496744857_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeESpvo5kTLW6bnUQ9kWt8n3McuOTp4BJdcxy45OngEl15Q6kiVsg1VwJRR_lMOTEbBfncCA1uELeK1Dfb58hB8N&_nc_ohc=3ht8pG3fMtUAX9EhApW&_nc_ht=scontent-cgk1-1.xx&oh=00_AT_AHqmbFUW0ubdfwENh_kymf4dznoK0DDT8tFNlMwBbBQ&oe=6306D1F7" style={{maxHeight:50}}></img>
  </button>
  <div className="dropdown-menu" >
  <Link href="/admin/login" ><a style={{textDecoration:'none' ,color:'black'}}>Logout</a></Link>
  <Link href="#" ><a style={{textDecoration:'none' ,color:'black'}}>Menu</a></Link>
  <Link href="#" ><a style={{textDecoration:'none' ,color:'black'}}>Menu</a></Link>
  </div>


  
</div>

    {/* <!-- Right elements --> */}
  </div>
  {/* <!-- Container wrapper --> */}
  </div>
</nav>
</div>
</>
   );
  }
export default Topbar