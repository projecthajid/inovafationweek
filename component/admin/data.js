import axios from "axios";
import { useEffect, useState } from "react";

const TableData =() => {
    const[posts, setPosts] = useState([]);
    const apiEndPoint ="https://jsonplaceholder.typicode.com/users";
    useEffect(()=> {

        const getPosts = async () => {
          
          const { data: res} = await axios.get(apiEndPoint);
          setPosts(res);
        };
        getPosts();
    },[] );

    // const addUser = async () => {
    //   const post = {title:"New Post", body:"new"}
    //   await axios.post(apiEndPoint, post);
    //   setPosts([post, ...posts]);
    // };

  return ( <>
    <div className="container">
      <h2>There are ada {posts.length} data</h2>
      
      <table className="table table-data">
          <thead>
            <th>Nama</th>
            <th>email</th>
            <th>phone</th>
            <th>Update</th>
            <th>Deleted</th>
          </thead>

          <tbody>
            {posts.map((post) => (
              <tr key={post.id}>
                <td>{post.name}</td>
                <td>{post.email}</td>
                <td>{post.phone}</td>
                <td><button className="btn btn-info">edit</button></td>
                <td><button className="btn btn-danger">delete</button></td>
                </tr>
              
             ) )}
          </tbody> 
      </table>
    </div>
  
  </> 
  );
};

export default TableData