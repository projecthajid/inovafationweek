import Link from "next/dist/client/link";


const BelanjaUser = () => { 


return (
<>

<div className="col-md-12 col-xl-3">
    <div className="card mini-stat" style={{ boxShadow: '0px 10px 5px 0px rgba(199,191,199,1)'}}>
    <div className="mini-stat-icon text-right">
      </div>
      <div className="p-5"><Link href="/user/product/telkomsel"><h5 className="text-uppercase">telkomsel</h5></Link>
      </div>
    </div>
  </div>

  <div className="col-md-12 col-xl-3">
    <div className="card mini-stat" style={{ boxShadow: '0px 10px 5px 0px rgba(199,191,199,1)'}}>
    <div className="mini-stat-icon text-right">
      </div>
      <div className="p-5"><Link href="/user/product/smartfren"><h5 className="text-uppercase">smartfren</h5></Link>
      </div>
    </div>
  </div>

  <div className="col-md-12 col-xl-3">
    <div className="card mini-stat" style={{ boxShadow: '0px 10px 5px 0px rgba(199,191,199,1)'}}>
    <div className="mini-stat-icon text-right">
      </div>
      <div className="p-5"><Link href="/user/product/axis"><h5 className="text-uppercase">axis</h5></Link>
      </div>
    </div>
  </div>

  <div className="col-md-12 col-xl-3">
    <div className="card mini-stat" style={{ boxShadow: '0px 10px 5px 0px rgba(199,191,199,1)'}}>
    <div className="mini-stat-icon text-right">
      </div>
      <div className="p-5"><Link href="/user/product/indosat"><h5 className="text-uppercase">indosat</h5></Link>
      </div>
    </div>
  </div>

</>
);
}


export default BelanjaUser  


