import Link from 'next/link';
import Image from 'next/image';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faAddressBook, faArchive, faHome, faPenAlt, faShoppingCart, faUserAlt, faUserEdit, } from "@fortawesome/free-solid-svg-icons";
function Sidebar() {

    return (
<>
<div className="left side-menu">
                   <div className="sidebar-inner slimscrollleft" id="sidebar-main">
                   <div className="topbar-left">
                       <div className="text-center mt-3">
                            <Link href={"/"} className="logo"><h2>MyVoucher</h2></Link>
                       </div>
                   </div>
                       <div id="sidebar-menu">
                           <ul>
                               <li className="menu-title pt-3">Menu</li>

                               <li>
                                    <Link href="/user/">
                                   <a className="waves-effect waves-light ms-4"><FontAwesomeIcon icon={	faHome} color={' rgba(89, 206, 181, 0.85)'} className='me-2'/><span> Dashboard</span></a></Link>
                               </li>
                              

                               <li>
                                    <Link href="/user/belanja">
                                   <a className="waves-effect waves-light ms-4"><FontAwesomeIcon icon={	faShoppingCart} color={' rgba(89, 206, 181, 0.85)'} className='me-2'/><span> beli paket </span></a></Link>
                               </li>

                             
                           </ul>
                       </div>
                   <div className="clearfix"></div>
               </div> 
           </div>
</>
   );
  }
export default Sidebar







