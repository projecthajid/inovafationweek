import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faSearch } from "@fortawesome/free-solid-svg-icons";

function Topbar() {

    return (
<>
<div className="topbar"  >
<nav className="navbar navbar-expand-lg navbar-light" style={{background:'#031554',boxShadow:  ' 0px 3px 13px 2px rgba(149,132,207,0.92)'}}>
  {/* <!-- Container wrapper --> */}
  <div className="container-fluid" >
    {/* <!-- Toggle button --> */}
    <button
      className="navbar-toggler"
      type="button"
      data-mdb-toggle="collapse"
      data-mdb-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
      
    >
      <i className="fas fa-bars"></i>
    </button>

    {/* <!-- Collapsible wrapper --> */}
    <div className="collapse navbar-collapse" id="navbarSupportedContent" >
      {/* <!-- Navbar brand --> */}
      <div className="navbar-brand  mt-lg-0" style={{height:50 }}>

      </div>

    </div>
    {/* <!-- Collapsible wrapper --> */}

    {/* <!-- Right elements --> */}
    <div className="d-flex align-items-center" >
    
      {/* <!-- Avatar --> */}
     
     
    
      <div className=" dropstart"  >
  <button type="button"  className="  " data-bs-toggle="dropdown" aria-expanded="" style={{background:'#031554'}}>
  <img className="rounded-circle" style={{maxHeight:50}} src="/avatar-1.jpg"></img>
  </button>
  <div className="dropdown-menu" >
  <Link href="/user/" ><a style={{textDecoration:'none' ,color:'black'}}>Home</a></Link>
  <Link href="/user/belanja" ><a style={{textDecoration:'none' ,color:'black'}}>Belanja</a></Link>
  <Link href="/loginUser" ><a style={{textDecoration:'none' ,color:'black'}}>logout</a></Link>
  </div>


  
</div>

    {/* <!-- Right elements --> */}
  </div>
  {/* <!-- Container wrapper --> */}
  </div>
</nav>
</div>
</>
   );
  }
export default Topbar